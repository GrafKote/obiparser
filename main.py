import pandas as pd
import requests
import re
from bs4 import BeautifulSoup as bs

# Инициализируем словарь для заполнения эксель файла
data_dict = {
    'Наименование': [],
    'Ссылка': [],
    'Цена': [],
}


def saveData(name, price, link):
    # Вносим данные в соотвествующие поля словаря
    data_dict['Наименование'].append(name)
    data_dict['Ссылка'].append(price)
    data_dict['Цена'].append(link)

    # Записываем данные в эксель файл
    data = pd.DataFrame(data_dict)
    data.to_excel('./data.xlsx')


def pageParse(soup):
    # Определяем массив из карточек объектов
    card = soup.find_all('a', class_='product-wrapper wt_ignore')
    print('Найдено карточек: ' + str(len(card)))

    # Перебираем карточки на странице и вытаскиваем из них название, цену и ссылку на товар
    for i in range(len(card)):
        name = card[i].find('span', class_='description').text
        price = card[i].find('span', class_='price').text
        price = re.sub(r'[^,\d]+', r'', price).replace(',', '.')
        link = 'https://www.obi.ru' + card[i].attrs['href']
        # Отладка
        # print(name)
        # print(price)
        # print(link)
        # Вызываем функцию, записывающую данные
        saveData(name, price, link)


def request(query):
    # Задаём стартовые параметры

    maxPage = 1
    page = 1
    while page < (maxPage + 1):
        # Задаём ссылку для перехода на сайт
        url = 'https://www.obi.ru/search/' + query + '/?page=' + str(page)

        # Запршариваем данные
        response = requests.get(url)
        if response.status_code == 200:
            print('Подключение удалось')

            # Получаем HTML страницу
            html = response.content
            # Распаршиваем её в bs
            soup = bs(html, 'html.parser')

            # Находим кол-во единц товара на странице и вычисляем кол-во страниц
            try:
                cnt = soup.find('div', class_='results-bar__variants hidden-phone hidden-tablet').text
                # Используем регулярное выражение, чтобы отчистить строку от всего, кроме цифр
                cnt = re.sub(r'[^\d]+', r'', cnt)
                print('Товаров найдено: ' + str(cnt))
                maxPage = (int(cnt) // 72) + 1

            except:
                print('Результатов не найдено')
                break

            page += 1

            # Вызываем функцию получения желаемых данных со страницы
            pageParse(soup)

        else:
            print('Запрос неудался, код ошибки: ' + str(response.status_code))


def main():
    # Запрос на поиск
    print('Введите запрос: ')
    query = str(input())
    print('Начинаю поиск по запросу ' + query + '...')
    request(query)
    print('Парсинг по запросу ' + query + ' завершен, результаты сохранены по пути ./data.xlsx')


if __name__ == '__main__':
    main()
